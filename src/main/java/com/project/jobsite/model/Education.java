package com.project.jobsite.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class Education {
	
	@Id
	@GeneratedValue
	@Column(name = "educationID", nullable = false)
	private int educationID;
	
//	@Column(name = "resumeID", nullable = false)
//	private int resumeID;
	
	@Column(name = "schoolname", nullable = false, length = 150)
	private String schoolname;
	
	@Column(name = "description", length = 255)
	private String description;
	
	@Column(name = "since", nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar since;
	
	@Column(name = "till")
	@Temporal(TemporalType.DATE)
	private Calendar till;
	
	@Column(name = "active", nullable = false)
	private int active;

	public int getEducationID() {
		return educationID;
	}

	public void setEducationID(int educationID) {
		this.educationID = educationID;
	}

//	public int getResumeID() {
//		return resumeID;
//	}
//
//	public void setResumeID(int resumeID) {
//		this.resumeID = resumeID;
//	}

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getSince() {
		return since;
	}

	public void setSince(Calendar since) {
		this.since = since;
	}

	public Calendar getTill() {
		return till;
	}

	public void setTill(Calendar till) {
		this.till = till;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	

}
