package com.project.jobsite.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name = "User")
public class User {
	
	@Id
	@GeneratedValue
	@Column(name = "userID", nullable = false)
	private int userID;
	
	@Column(name = "login", nullable = false, length = 20)
	private String login;
	
	@Column(name = "password", nullable = false, length = 30)
	private String password;
	
	@Column(name = "email", nullable = false, length = 50)
	private String email;
	
	@Column(name = "status", nullable = false)
	private int status;	
	
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@ManyToMany
	private List<Resume> resumes;

	public List<Resume> getResumes() {
		return resumes;
	}
	public void setResumes(List<Resume> resumes) {
		this.resumes = resumes;
	}
	
	@ManyToMany
	private List<Offert> offerts;

	public List<Offert> getOfferts() {
		return offerts;
	}
	public void setOfferts(List<Offert> offerts) {
		this.offerts = offerts;
	}
	
	@ManyToMany
	private List<Company> companies;

	public List<Company> getCompanies() {
		return companies;
	}
	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	
	

}
